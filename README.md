# 5 minutes production app with Vue

![AWS EC2 Deployment](doc/images/deployed.png)

## Resources to learn VueJs

- [First app](https://flaviocopes.com/vue-first-app/)
- [Vue Cli](https://flaviocopes.com/vue-cli/)
- [.gitignore for Vue](https://medium.com/@renatello/things-i-put-in-gitignore-file-for-vue-js-projects-c53cecd8bd08)
- [Docker image](https://vuejs.org/v2/cookbook/dockerize-vuejs-app.html)

More exercises:

- [Add SCSS](https://vue-loader.vuejs.org/guide/pre-processors.html#less)

## Resources for 5 min prod app deploy

- [Deploy template](https://gitlab.com/gitlab-org/5-minute-production-app/deploy-template)
- [Blog: First code to CI/CD deployments in 5 minutes?](https://about.gitlab.com/blog/2020/12/15/first-code-to-ci-cd-deployments-in-5-minutes/)

Tasks done:

- AWS credentials added to the project settings
- CI template in .gitlab-ci.yml

## Development

Manual container build:


```shell
docker build -t 5-min-prod-app-vuejs .
```

## Development

### Project setup
```
yarn install
```

#### Compiles and hot-reloads for development
```
yarn serve
```

#### Compiles and minifies for production
```
yarn build
```

#### Lints and fixes files
```
yarn lint
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
