// vue.config.js
module.exports = {
    // Rationale: https://github.com/gitpod-io/gitpod/issues/26#issuecomment-554058232 
    devServer: {
        disableHostCheck: true
    }
}